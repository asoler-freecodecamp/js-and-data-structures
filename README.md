# Javascript and Data Structures Projects

These are the FreeCodeCamp Challenges for the JavaScript Algorithms and Data Structures Certification

The Projects should be built in the [FreeCodeCamp](https://www.freecodecamp.org/) website

Each folder contains a project separated with a js file containing the function that should be implemented and some calls to test the function.
