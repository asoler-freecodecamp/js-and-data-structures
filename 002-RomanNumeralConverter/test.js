function convertToRoman(num) {
  const values = {
    1000: 'M',
    900: 'CM',
    500: 'D',
    400: 'CD',
    100: 'C',
    90: 'XC',
    50: 'L',
    40: 'XL',
    10: 'X',
    9: 'IX',
    5: 'V',
    4: 'IV',
    1: 'I'
  };
  let result = "";
  let currentNum = num;
  let boundaries = Object.keys(values).reverse().map(x => parseInt(x));
  for (let i = 0; i < boundaries.length; i++) {
    const boundary = boundaries[i];
    if (currentNum >= boundary) {
      while (currentNum >= boundary) {
        result += values[boundary];
        currentNum -= boundary;
      }
    }
  }
  return result;
}


console.log('convertToRoman(2) should return "II".')
console.assert(convertToRoman(2) === 'II', 'convertToRoman(2) FAILED to return "II"')

console.log('convertToRoman(3) should return "III".')
console.assert(convertToRoman(3) === 'III', 'convertToRoman(3) FAILED to return "III"')

console.log('convertToRoman(4) should return "IV".')
console.assert(convertToRoman(4) === 'IV', 'convertToRoman(4) FAILED to return "IV"')

console.log('convertToRoman(5) should return "V".')
console.assert(convertToRoman(5) === 'V', 'convertToRoman(5) FAILED to return "V"')

console.log('convertToRoman(9) should return "IX".')
console.assert(convertToRoman(9) === 'IX', 'convertToRoman(9) FAILED to return "IX"')

console.log('convertToRoman(12) should return "XII".')
console.assert(convertToRoman(12) === 'XII', 'convertToRoman(12) FAILED to return "XII"')

console.log('convertToRoman(16) should return "XVI".')
console.assert(convertToRoman(16) === 'XVI', 'convertToRoman(16) FAILED to return "XVI"')

console.log('convertToRoman(29) should return "XXIX".')
console.assert(convertToRoman(29) === 'XXIX', 'convertToRoman(29) FAILED to return "XXIX"')

console.log('convertToRoman(44) should return "XLIV".')
console.assert(convertToRoman(44) === 'XLIV', 'convertToRoman(2) FAILED to return "XLIV"')

console.log('convertToRoman(45) should return "XLV"')
console.assert(convertToRoman(45) === 'XLV', 'convertToRoman(44) FAILED to return "XLV"')

console.log('convertToRoman(68) should return "LXVIII"')
console.assert(convertToRoman(68) === 'LXVIII', 'convertToRoman(68) FAILED to return "LXVIII"')

console.log('convertToRoman(83) should return "LXXXIII"')
console.assert(convertToRoman(83) === 'LXXXIII', 'convertToRoman(2) FAILED to return "LXXXIII"')

console.log('convertToRoman(97) should return "XCVII"')
console.assert(convertToRoman(97) === 'XCVII', 'convertToRoman(83) FAILED to return "XCVII"')

console.log('convertToRoman(99) should return "XCIX"')
console.assert(convertToRoman(99) === 'XCIX', 'convertToRoman(99) FAILED to return "XCIX"')

console.log('convertToRoman(400) should return "CD"')
console.assert(convertToRoman(400) === 'CD', 'convertToRoman(400) FAILED to return "CD"')

console.log('convertToRoman(500) should return "D"')
console.assert(convertToRoman(500) === 'D', 'convertToRoman(500) FAILED to return "D"')

console.log('convertToRoman(501) should return "DI"')
console.assert(convertToRoman(501) === 'DI', 'convertToRoman(501) FAILED to return "DI"')

console.log('convertToRoman(649) should return "DCXLIX"')
console.assert(convertToRoman(649) === 'DCXLIX', 'convertToRoman(649) FAILED to return "DCXLIX"')

console.log('convertToRoman(798) should return "DCCXCVIII"')
console.assert(convertToRoman(798) === 'DCCXCVIII', 'convertToRoman(798) FAILED to return "DCCXCVIII"')

console.log('convertToRoman(891) should return "DCCCXCI"')
console.assert(convertToRoman(891) === 'DCCCXCI', 'convertToRoman(891) FAILED to return "DCCCXCI"')

console.log('convertToRoman(1000) should return "M"')
console.assert(convertToRoman(1000) === 'M', 'convertToRoman(1000) FAILED to return "M"')

console.log('convertToRoman(1004) should return "MIV"')
console.assert(convertToRoman(1004) === 'MIV', 'convertToRoman(1004) FAILED to return "MIV"')

console.log('convertToRoman(1006) should return "MVI"')
console.assert(convertToRoman(1006) === 'MVI', 'convertToRoman(1006) FAILED to return "MVI"')

console.log('convertToRoman(1023) should return "MXXIII"')
console.assert(convertToRoman(1023) === 'MXXIII', 'convertToRoman(1023) FAILED to return "MXXIII"')

console.log('convertToRoman(2014) should return "MMXIV"')
console.assert(convertToRoman(2014) === 'MMXIV', 'convertToRoman(2014) FAILED to return "MMXIV"')

console.log('convertToRoman(3999) should return "MMMCMXCIX"')
console.assert(convertToRoman(3999) === 'MMMCMXCIX', 'convertToRoman(3999) FAILED to return "MMMCMXCIX"')

