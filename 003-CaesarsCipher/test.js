function rot13(str) {
  const arr = str.split('');
  let result = "";
  for (let i = 0; i < arr.length; i++) {
    const currentAscii = arr[i].charCodeAt(0);
    if (currentAscii >= 65 && currentAscii <= 90) {
      if (currentAscii >= 78) {
        result += String.fromCharCode(currentAscii - 13);
      } else {
        result += String.fromCharCode(currentAscii + 13);
      }
    } else {
      result += arr[i];
    }
  }
  return result;
}

console.log('rot13("SERR PBQR PNZC") should decode to FREE CODE CAMP')
console.assert(rot13("SERR PBQR PNZC") === 'FREE CODE CAMP', 'rot13("SERR PBQR PNZC") FAILED to decode to FREE CODE CAMP')

console.log('rot13("SERR CVMMN!") should decode to FREE PIZZA!')
console.assert(rot13("SERR CVMMN!") === 'FREE PIZZA!', 'rot13("SERR CVMMN!") FAILED to decode to FREE PIZZA!')

console.log('rot13("SERR YBIR?") should decode to FREE LOVE?')
console.assert(rot13("SERR YBIR?") === 'FREE LOVE?', 'rot13("SERR YBIR?") FAILED to decode to FREE LOVE?')

console.log('rot13("GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT.") should decode to THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.')
console.assert(rot13("GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT.") === 'THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.', 'rot13("GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT.") FAILED to decode to THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.')
