function palindrome(str) {
  const onlyAlpha = str.replace(/[\W_]/g, "").toLowerCase();
  const reversedString = onlyAlpha.split("").reverse().join("");
  return onlyAlpha === reversedString
}

console.log('palindrome("eye") should return true.')
console.assert(palindrome("eye") === true, 'palindrome("eye") should return true.')

console.log('palindrome("_eye") should return true.')
console.assert(palindrome("_eye") === true, 'palindrome("_eye") should return true.')

console.log('palindrome("race car") should return true.')
console.assert(palindrome("race car") === true, 'palindrome("race car") should return true.')

console.log('palindrome("A man, a plan, a canal. Panama") should return true.')
console.assert(palindrome("A man, a plan, a canal. Panama") === true, 'palindrome("A man, a plan, a canal. Panama") should return true.')

console.log('palindrome("never odd or even") should return true.')
console.assert(palindrome("never odd or even") === true, 'palindrome("never odd or even") should return true.')

console.log('palindrome("My age is 0, 0 si ega ym.") should return true.')
console.assert(palindrome("My age is 0, 0 si ega ym.") === true, 'palindrome("My age is 0, 0 si ega ym.") should return true.')

console.log('palindrome("0_0 (: /-\\ :) 0-0") should return true.')
console.assert(palindrome("0_0 (: /-\\ :) 0-0") === true, 'palindrome("0_0 (: /-\\ :) 0-0") should return true.')


console.log('palindrome("not a palindrome") should return false.')
console.assert(palindrome("not a palindrome") === false, 'palindrome("not a palindrome") should return true.')

console.log('palindrome("nope") should return false.')
console.assert(palindrome("nope") === false, 'palindrome("nope") should return true.')

console.log('palindrome("almostomla") should return false.')
console.assert(palindrome("almostomla") === false, 'palindrome("almostomla") should return true.')

console.log('palindrome("1 eye for of 1 eye.") should return false.')
console.assert(palindrome("1 eye for of 1 eye.") === false, 'palindrome("1 eye for of 1 eye.") should return true.')

console.log('palindrome("five|\\_/|four") should return false.')
console.assert(palindrome("five|\\_/|four") === false, 'palindrome("five|\\_/|four") should return true.')
