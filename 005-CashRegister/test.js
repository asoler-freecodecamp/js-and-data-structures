function checkCashRegister(price, cash, cid) {
  let result = [];
  let status = 'OPEN';
  const unitsArray =
      [
        ['ONE HUNDRED', 100],
        ['TWENTY', 20],
        ['TEN', 10],
        ['FIVE', 5],
        ['ONE', 1],
        ['QUARTER', 0.25],
        ['DIME', 0.1],
        ['NICKEL', 0.05],
        ['PENNY', 0.01]
      ];

  let changeAmount = cash - price;
  // console.log(changeAmount);
  if (changeAmount >= 0) {
    for (let item in unitsArray) {
      const currentCid = cid.filter(cid => cid[0] === unitsArray[item][0]);
      //console.log(currentCid[0]);
      const currentCidMaxAvailable = Math.floor(currentCid[0][1] / unitsArray[item][1]);
      const count = Math.floor(changeAmount / unitsArray[item][1]);
      const currentCount = Math.min(count, currentCidMaxAvailable);
      if (currentCount > 0) {
        const amount = currentCount * unitsArray[item][1];
        // console.log(changeAmount, currentCidMaxAvailable, count, amount);
        changeAmount = Math.round((100 + Number.EPSILON) * (changeAmount - amount)) / 100;
        result.push([unitsArray[item][0], amount]);
      }
    }
  }
  if (changeAmount > 0) {
    status = 'INSUFFICIENT FUNDS';
    result = [];
  } else {
    const totalInCid = cid.map(item => item[1]).reduce((accumulator, item) => accumulator + item);
    if (totalInCid <= cash - price) {
      status = 'CLOSED';
    }
  }
  //console.log(cid.map(item => item[1]).reduce((accumulator, item) => accumulator + item));
  let returnValue = {status: status, change: result};
  console.log(returnValue);
  return returnValue;
}

checkCashRegister(19.5, 20, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]);
checkCashRegister(3.26, 100, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]);
checkCashRegister(19.5, 20, [["PENNY", 0.01], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]])
checkCashRegister(19.5, 20, [["PENNY", 0.01], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 1], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]])
checkCashRegister(19.5, 20, [["PENNY", 0.5], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]])
