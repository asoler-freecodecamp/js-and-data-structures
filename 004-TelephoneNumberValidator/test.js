
function telephoneCheck(str) {
  const re = /^([+]?1[\s]?)?((?:[(](?:[2-9]1[02-9]|[2-9][02-8][0-9])[)][\s]?)|(?:(?:[2-9]1[02-9]|[2-9][02-8][0-9])[\s.-]?)){1}([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2}[\s.-]?){1}([0-9]{4}){1}$/;
  return re.test(str);
}

console.log('Test 555-555-555 is valid')
console.assert(telephoneCheck('555-555-5555')===true, "555-555-5555 should be valid")

console.log('Test 1 555-555-5555 is valid')
console.assert(telephoneCheck('1 555-555-5555')===true, "1 555-555-5555 should be valid")

console.log('Test 1 (555) 555-5555 is valid')
console.assert(telephoneCheck('1 (555) 555-5555')===true, "1 (555) 555-5555 should be valid")

console.log('Test 5555555555 is valid')
console.assert(telephoneCheck('5555555555')===true, "5555555555 should be valid")

console.log('Test (555)555-5555is valid')
console.assert(telephoneCheck('(555)555-5555')===true, "(555)555-5555 should be valid")

console.log('Test 1(555)555-5555')
console.assert(telephoneCheck('1(555)555-5555')===true, "1(555)555-5555 should be valid")

console.log('Test 1 456 789 4444')
console.assert(telephoneCheck('1 456 789 4444')===true, "1 456 789 4444 should be valid")



console.log('Test 555-5555 is invalid')
console.assert(telephoneCheck('555-5555')===false, "555-5555 should be invalid")

console.log('Test 5555555 is invalid')
console.assert(telephoneCheck('5555555')===false, "5555555 should be invalid")

console.log('Test 1 555)555-5555 is invalid')
console.assert(telephoneCheck('1 555)555-5555')===false, "1 555)555-5555 should be invalid")

console.log('Test 123**&!!asdf# is invalid')
console.assert(telephoneCheck('123**&!!asdf#')===false, "123**&!!asdf# should be invalid")

console.log('Test 55555555 is invalid')
console.assert(telephoneCheck('55555555')===false, "55555555 should be invalid")

console.log('Test (6054756961) is invalid')
console.assert(telephoneCheck('(6054756961)')===false, "(6054756961) should be invalid")

console.log('Test 2 (757) 622-7382 is invalid')
console.assert(telephoneCheck('2 (757) 622-7382')===false, "2 (757) 622-7382 should be invalid")

console.log('Test 10 (757) 622-7382 is invalid')
console.assert(telephoneCheck('10 (757) 622-7382')===false, "10 (757) 622-7382 should be invalid")

console.log('Test 27576227382 is invalid')
console.assert(telephoneCheck('27576227382')===false, "27576227382 should be invalid")

console.log('Test (275)76227382 is invalid')
console.assert(telephoneCheck('(275)76227382')===false, "(275)76227382 should be invalid")

console.log('Test 2(757)6227382 is invalid')
console.assert(telephoneCheck('2(757)6227382')===false, "2(757)6227382 should be invalid")

console.log('Test 2(757)622-7382 is invalid')
console.assert(telephoneCheck('2(757)622-7382')===false, "2(757)622-7382 should be invalid")

console.log('Test 555)-555-5555 is invalid')
console.assert(telephoneCheck('555)-555-5555')===false, "555)-555-5555 should be invalid")

console.log('Test (555-555-5555 is invalid')
console.assert(telephoneCheck('(555-555-5555')===false, "(555-555-5555 should be invalid")

console.log('Test (555)5(55?)-555 is invalid')
console.assert(telephoneCheck('(555)5(55?)-555')===false, "(555)5(55?)-555 should be invalid")
